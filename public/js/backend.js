$(function () {
	// http://whitenight.work/overview.aspx
	// Type = 1(access_token)
	// Type = 2(CameraUID)
	// Type = 3(每日人流數據)
	// Type = 4(客群性別)
	// Type = 5(客群年記)
	// var apiDomain = 'https://whitenight.work';
	var apiDomain = 'https://demotlvm.site';
	//var apiDomain = 'https://localhost:44381';
	$.ajaxSetup({
		async: false,
		crossDomain: true,
		beforeSend: function (xhr, options) {
			options.url = apiDomain + options.url;
		}
	});

	//關閉任何的lightbox
	$("#mask,.lg-bot .btn-cancel").on("click", function () {
		$("#mask,.lg-box").fadeOut(300);
	});

	//打開lightbox
	$("#btnOpenMesg").on("click", function () {
		$("#lgMesg,#mask").fadeIn(300);
	});
	$("#btnOpenBig").on("click", function () {
		$("#lgBig,#mask").fadeIn(300);
	});

	//側邊欄選單
	$('#closeNav').on('click', function () {
		$('#closeNav').animate({ left: '-26px' }, 300);
		$('.side-nav').animate({ width: '0' }, 300);
		$('.btn-side-open').animate({ left: '0' }, 400);
	});

	$('#openNav').on('click', function () {
		$('#closeNav').animate({ left: '180px' }, 300);
		$('.side-nav').animate({ width: '180px' }, 300);
		$('.btn-side-open').animate({ left: '-26px' }, 400);
	});

	//上傳檔案
	// $('#uploadFile').change(function () {
	// 	var filename = $(this).val();
	// 	var lastIndex = filename.lastIndexOf("\\");
	// 	if (lastIndex >= 0) {
	// 		filename = filename.substring(lastIndex + 1);
	// 	}
	// 	$('#fileNameBox').text(filename);
	// });

	$('[uploadfile]').change(function () {
		var filename = $(this).val();
		var lastIndex = filename.lastIndexOf("\\");
		if (lastIndex >= 0) {
			filename = filename.substring(lastIndex + 1);
		}
		$(this).parent().parent().find('span').text(filename);
	});

	//= 有刪除的上傳檔案 =
	$('[uploadfileHasDel]').change(function () {
		var filename = $(this).val();
		var lastIndex = filename.lastIndexOf("\\");
		if (lastIndex >= 0) {
			filename = filename.substring(lastIndex + 1);
		}
		$(this).parent().parent().find('span').html(filename + ' <i class="fas fa-times-square" uploadfiledel></i>');

		$('#fileLoading').fadeIn();
		// 獲取選中的文件
		var file = this.files[0];

		// 檢查是否有文件被選擇
		if (file) {
			// 使用 FileReader 來讀取文件
			var reader = new FileReader();

			// 當文件讀取完成時觸發
			reader.onload = function (e) {
				// 隱藏加載提示
				$('#fileLoading').fadeOut();
				// alert('文件加載完成！');
			};

			// 當讀取發生錯誤時觸發
			reader.onerror = function (e) {
				$('#fileLoading').fadeOut();
				alert('文件讀取錯誤，請重試！');
			};

			// 開始讀取文件（這裡使用 readAsDataURL，可以根據需求選擇不同的方法）
			reader.readAsDataURL(file);
		} else {
			// 沒有選擇文件，隱藏提示
			$('#fileLoading').hide();
		}
	});
	$(document).on('click', '[uploadfiledel]', (e) => {
		$(e.target).parent().parent().find('input').val('');
		$(e.target).parent().html('');
	});

	//增加標籤
	$('#btnTagAdd').on('click', function () {
		$('#tagCreat').show();
	});
	$('#btnTagAdd2').on('click', function () {
		$('#tagCreat2').show();
	});

	//切換中英輸入匡
	var $transBox = $('#trans1');
	$('[transBox]').find('[enBtn]').on('click', function () {
		var hasAct = $(this).hasClass('act');
		if (hasAct) {
			$(this).removeClass('act');
			$(this).parent().parent().find('[zhTrans]').show();
			$(this).parent().parent().find('[enTrans]').hide();
		} else {
			$(this).addClass('act');
			$(this).parent().parent().find('[zhTrans]').hide();
			$(this).parent().parent().find('[enTrans]').show();
		}
	});

});
// $(window).load(function () {
// 	//側邊欄nav height
// 	sideNav();
// })
$(window).resize(function () {
	sideNav();
});

//上傳圖片的按鈕
function upload_click(obj) {
	var fileEvent = $(obj).parent().find('input[type=file]');
	fileEvent.click();
}

//側邊欄nav height
function sideNav() {
	var _navH = $('.side-nav').height();
	var _logo = $('.side-logo').outerHeight();
	var _tit = $('.backend-tit').outerHeight();
	var _info = $('.user-info').outerHeight();
	var _sideFoot = $('.side-footer').outerHeight();
	//console.log( _navH,_logo,_tit,_info,_sideFoot);
	$('.nav-box').height(_navH - (_logo + _tit + _info + _sideFoot + 50));
}


//=== lg內選過的tag顯示至列表 ===
//= lg input area, tag 頁面顯示匡 , 整體 lg
// pushTag('#authorList','#authorTagArea','#lgAuthorTagBox');
function pushTag(lgList, tagArea, lgTagBox) {
	var $lgList = $(lgList);
	var $tagArea = $(tagArea);
	// var $lgTagBox = $(lgTagBox);
	var tagList = [];
	//= lg內容盒 =
	$lgList.find('input:checked').each(function (ind, item) {
		var tagId = $(item).attr('id');
		var tagName = $(item).parent().find('span').html();
		tagList.push({ 'tagId': tagId, 'tagName': tagName });
	});
	//== 清除顯示匡的tag ==
	$tagArea.html('');
	$.each(tagList, function (ind, val) {
		var tag = '<li>' +
			'<div class="tag-name">' + val.tagName + '</div>' +
			'<div class="tag-del" data-del="' + val.tagId + '">' +
			'<i class="fad fa-times" title="刪除" alt="刪除"></i>' +
			'</div>' +
			'</li>';
		$tagArea.append(tag);
	});
	//=== 是否顯示無資料 ===
	tagNoData(tagArea);
	$(lgTagBox + ',#mask').fadeOut();
	//= 加入tag後綁定點擊功能 刪除功能 =
	$tagArea.find('.tag-del').on('click', function () {
		var delId = $(this).data('del');
		console.log(delId);
		$(this).parent().remove();
		//= 消除lg的勾勾狀態 =
		$lgList.find('#' + delId).prop('checked', false);
		//=== 是否顯示無資料 ===
		tagNoData(tagArea);
	});
}
//=== 是否顯示無資料 ===
function tagNoData(tagArea) {
	var $tagArea = $(tagArea);
	var tagL = $tagArea.find('li').length;
	//console.log(tagL);
	tagL == 0 ? $tagArea.next('.no-data').show() : $tagArea.next('.no-data').hide();
	tagL !== 0 && $('#tagNote').remove();
}


//我的部分
function resetForm() {
	$("#form").find('input:file').each(function (evt) {
		$(this).val('');
	});
}

//takes an array of JavaScript File objects
function getFilesDetail(files) {
	return Promise.all(files.map(file => getFileDetail(file)));
}

//take a single JavaScript File object
function getFileDetail(file) {
	var reader = new FileReader();
	return new Promise((resolve, reject) => {
		reader.onerror = () => { reader.abort(); reject(new Error("Error parsing file")); }
		reader.onload = function () {

			//This will result in an array that will be recognized by C#.NET WebApi as a byte[]
			let bytes = Array.from(new Uint8Array(this.result));

			//if you want the base64encoded file you would use the below line:
			let base64StringFile = btoa(bytes.map((item) => String.fromCharCode(item)).join(""));

			//Resolve the promise with your custom file structure
			resolve({
				bytes: bytes,
				base64StringFile: base64StringFile,
				fileName: file.name,
				fileType: file.type
			});
		}
		try {
			reader.readAsArrayBuffer(file);
		} catch (e) { }
	});
}
//--------------------------------------