var cameraList = [];
var cameraShowList = [];
var csvData, csvData2, csvFileName;
$(function () {
    //時間選擇器
    var yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    //=== 日期格式 ===dateRange
    $("#dateRange").datepicker({
        dateFormat: "yyyy/mm/dd",
        autoClose: true,
        position: "bottom right",
        language: 'en',
        range: true,
        multipleDatesSeparator: ' ~ ',
        maxDate: yesterday,
        // minDate: threeMonth,
        onSelect: (fd, d, picker) => {
            //- this.date = this.$("#date").val();
        },
    });
    // var currentDate = new Date();
    // $("#dateRange").data('datepicker').selectDate(yesterday);
    //取消因為滾動不會消失的日期選取器
    $('.main').scroll(function () {
        $('.datepickers-container').find('.datepicker').removeClass('active');
        $('.datepickers-container').find('.datepicker').css({ 'left': '-100000px' });
        $('#dateRange').blur();
    });

    //=== 取得API ===
    //== 取的攝影機列表 ==
    // $('#type').html('');
    var apiLink = '/api/overview?Type=2'
    // var cameraList = [];
    $.get(apiLink, function (data) {
        var aipData = JSON.parse(data);
        console.log(aipData);
        $.each(aipData, function (ind, val) {
            var { CameraUID, CameraName } = val;
            //$('#type').append('<option value="' + CameraUID + '">' + CameraName + '</option>');
            //== 複數 ==
            cameraList.push({ CameraUID, CameraName });
        });
    });

    //== 路徑資料 ==
    // var cameraId = 'a6aacd70dbc84b9c9e434640b38a17d2';
    // var apiLink = '/data_analysis_path.aspx?Type=2&Sday=2021-05-05&Eday=2021-06-01&CameraUID=' + cameraId;
    $('#srcBtn').on('click', function () {
        var dateRange = $('#dateRange').val();
        if (dateRange == '') {
            alert('請選擇日期。')
        } else {
            srcStart();
        }
    });

    //=== 圖片 csv匯出 ===
    //== 圖片 ==
    var exportList = [
        { 'btn': '#exportBtn1', 'imgCont': 'pathResult01', 'fileName': '正大門' },
        { 'btn': '#exportBtn2', 'imgCont': 'pathResult02', 'fileName': '電梯口' },
        { 'btn': '#exportBtn3', 'imgCont': 'pathResult03', 'fileName': '文學咖啡坊' },
    ]
    $.each(exportList, function (ind, val) {
        var $btn = $(val.btn);
        $btn.on('click', function () {
            html2canvas(document.getElementById(val.imgCont)).then(function (canvas) {
                var anchorTag = document.createElement("a");
                document.body.appendChild(anchorTag);
                document.getElementById("previewImg").appendChild(canvas);
                anchorTag.download = val.fileName + ".jpg";
                anchorTag.href = canvas.toDataURL();
                anchorTag.target = '_blank';
                anchorTag.click();
            });
        });
    });
    //== csv ==
    $('#csvBtn').on('click', function () {
        urlTableToExcel();
    });
});
//=== 啟動搜尋 ===
function srcStart() {
    var type = $('#type').val();
    var dateRange = $('#dateRange').val();
    var sumUnitRange = $('#range').val();
    var sday, eday, srcDayTxt;
    var dateRangeL = dateRange.split(' ~ ');
    sday = dateRangeL[0];
    dateRangeL.length > 1 ? eday = dateRangeL[1] : eday = dateRangeL[0];
    sday = sday.split('/').join('-');
    eday = eday.split('/').join('-');
    console.log('type:', type);
    console.log('dateRange:', sday, eday);
    dateRangeL.length > 1 ? srcDayTxt = dateRangeL[0] + ' - ' + dateRangeL[1] : srcDayTxt = dateRangeL[0];
    getCameraShow(type);
    console.log('cameraShowList', cameraShowList);
    // srcChart(type, sday, eday, srcDayTxt);
    //= 重置csv資料 =
    csvData = [];
    csvData2 = [];
    //= 置入當前選擇數據項目 =
    var typeName = $('#type').find('option:selected').text();
    csvFileName = typeName;
    //= 取得路徑資料 =
    if (type == 1 || type == 3) {
        getPathData(type, cameraShowList[0], sday, eday, sumUnitRange);
    } else if (type == 2) {
        getPathData2(cameraShowList[0], cameraShowList[1], sday, eday, sumUnitRange);
    }
    //= 開啟匯出圖片按鈕 =
    $('.btn-green').hide();
    if (type == 1) {
        $('#exportBtn1,#csvBtn').show();
    } else if (type == 2) {
        $('#exportBtn2,#csvBtn').show();
    } else {
        $('#exportBtn3,#csvBtn').show();
    }
}
//== 返回路徑所需攝影機 ==
function getCameraShow(type) {
    cameraShowList = [];
    $.each(cameraList, function (ind, val) {
        if (type == 1 && val.CameraUID == 'fca8b3ed1388467e83afffb3c2ba0a10') {
            //= 正大門 192.168.200.201 = 新修改
            //= 正大門 a6aacd70dbc84b9c9e434640b38a17d2 = 舊的
            //= 正大門 fca8b3ed1388467e83afffb3c2ba0a10 = 新的
            var cId = val.CameraUID;
            cameraShowList.push(cId);
        } else if (type == 2) {
            if (val.CameraUID == '6878611cc9a84a2c992d07a1c1a5e307') {
                //= 電梯口(近咖啡坊) 192.168.200.202 = 新修改
                //= 電梯口(近咖啡坊) 6878611cc9a84a2c992d07a1c1a5e307 =
                var cId = val.CameraUID;
                cameraShowList.push(cId);
            } else if (val.CameraUID == 'f8f9824543e24613a295d89e2750ad73') {
                //= 電梯口(近文藝大廳) 192.168.200.203 = 新修改
                //= 電梯口(近文藝大廳) f8f9824543e24613a295d89e2750ad73 =
                var cId = val.CameraUID;
                cameraShowList.push(cId);
            }
        } else if (type == 3 && val.CameraUID == '67bdb795665d4c93aace2cda365e4ba6') {
            //= 文學咖啡坊 192.168.200.204 = 新修改
            //= 文學咖啡坊 67bdb795665d4c93aace2cda365e4ba6 =
            var cId = val.CameraUID;
            cameraShowList.push(cId);
        }
    });
    // if (type == 1) {
    //     var cId = cameraList[2].CameraUID;
    //     cameraShowList.push(cId);
    // } else if (type == 2) {
    //     var cId = cameraList[3].CameraUID;
    //     cameraShowList.push(cId);
    //     var cId2 = cameraList[4].CameraUID;
    //     cameraShowList.push(cId2);
    // } else if (type == 3) {
    //     var cId = cameraList[0].CameraUID;
    //     cameraShowList.push(cId);
    // }
}
//== 取得路徑茲資料 1和3==
function getPathData(type, cameraId, sDay, eDay, unitRange) {
    // var cameraId = 'a6aacd70dbc84b9c9e434640b38a17d2';
    // var apiLink2 = '/data_analysis_path.aspx?Type=2&Sday=2021-05-05&Eday=2021-06-01&CameraUID=' + cameraId;
    var apiLink2 = '/api/dataanalysispath?Type=' + unitRange + '&Sday=' + sDay + '&Eday=' + eDay + '&CameraUID=' + cameraId;
    var pathData;
    $.get(apiLink2, function (data) {
        var aipData = JSON.parse(data);
        console.log(aipData);
        pathData = aipData;
        //= 匯出csv使用 =
        csvData = aipData;
    });
    console.log('pathData', pathData);

    $('.path-result-box').hide();
    $('#pathTable').show();
    $('#pathTable').html('<tr>' +
        '<th> 路徑名稱</th>' +
        '<th>人數</th>' +
        '</tr>');
    if (type == 1) {
        //左01 右02 前03 斜前04
        $('#pathResult01').show();
        $('#pathTxtA01,#pathTxtA02,#pathTxtA03,#pathTxtA04,#pathTxtA05').html(' - ');
        $('#pathA01,#pathA02,#pathA03,#pathA04,#pathA05').html(' - ');
        $.each(pathData, function (ind, val) {
            var { roi_uid, CameraName, Acount, AvgCount } = val;
            var indNum = ind + 1;
            //$('#pathA0' + indNum).html(Number(AvgCount).toFixed(2) + '%');
            // $('#pathTxtA0' + indNum).html(CameraName);
            console.log(CameraName);
            if (roi_uid == 'fca8b3ed1388467e83afffb3c2ba0a12') {//CameraName == '往常設展' 舊 a6aacd70dbc84b9c9e41625824779565
                $('#pathTxtA01').html(CameraName);
                $('#pathA01').html(Number(AvgCount).toFixed(2) + '%');
            } else if (roi_uid == 'fca8b3ed1388467e83afffb3c2ba0a15') {//CameraName == '往展覽室Ｄ' a6aacd70dbc84b9c9e41625824949866
                $('#pathTxtA02').html(CameraName);
                $('#pathA02').html(Number(AvgCount).toFixed(2) + '%');
            } else if (roi_uid == 'fca8b3ed1388467e83afffb3c2ba0a14') {//CameraName == '往二樓' a6aacd70dbc84b9c9e41625824895482
                $('#pathTxtA03').html(CameraName);
                $('#pathA03').html(Number(AvgCount).toFixed(2) + '%');
            } else if (roi_uid == 'fca8b3ed1388467e83afffb3c2ba0a13') {//CameraName == '往服務台' a6aacd70dbc84b9c9e41625824827361
                $('#pathTxtA04').html(CameraName);
                $('#pathA04').html(Number(AvgCount).toFixed(2) + '%');
            } else if (roi_uid == 'fca8b3ed1388467e83afffb3c2ba0a17') {//CameraName == '往服務台 2' a6aacd70dbc84b9c9e41641365339064
                $('#pathTxtA05').html(CameraName);
                $('#pathA05').html(Number(AvgCount).toFixed(2) + '%');
            }
            $('#pathTable').append('<tr>' +
                '<th>' + CameraName + '</th>' +
                '<th>' + FormatNumber(Acount) + '</th>' +
                '</tr>');
        });
        // pathData.splice(1, 1);
        // pathData.splice(3, 1);
        // console.log(pathData);
    } else if (type == 3) {
        //右01 左02 前03
        $('#pathResult03').show();
        $('#pathC01,#pathC02,#pathC03,#pathC04').html(' - ');
        $('#pathTxtC01,#pathTxtC02,#pathTxtC3,#pathTxtC4').html(' - ');
        $.each(pathData, function (ind, val) {
            var { roi_uid, CameraName, Acount, AvgCount } = val;
            var indNum = ind + 1;
            if (roi_uid == '67bdb795665d4c93aac1625827986919') {//CameraName == '往展覽室C'
                $('#pathC01').html(Number(AvgCount).toFixed(2) + '%');
                $('#pathTxtC01').html(CameraName);
            } else if (roi_uid == '67bdb795665d4c93aac1625827939612') {//CameraName == '往文資中心'
                $('#pathC03').html(Number(AvgCount).toFixed(2) + '%');
                $('#pathTxtC03').html(CameraName);
            } else if (roi_uid == '67bdb795665d4c93aac1625828027861') {//CameraName == '往廁所'
                $('#pathC04').html(Number(AvgCount).toFixed(2) + '%');
                $('#pathTxtC04').html(CameraName);
            }
            // $('#pathC0' + indNum).html(Number(AvgCount).toFixed(2) + '%');
            // $('#pathTxtC0' + indNum).html(CameraName);
            $('#pathTable').append('<tr>' +
                '<th>' + CameraName + '</th>' +
                '<th>' + FormatNumber(Acount) + '</th>' +
                '</tr>');
        });
    }
}

//== 取得路徑資料 2==
function getPathData2(cameraId, cameraId2, sDay, eDay, unitRange) {
    // var cameraId = 'a6aacd70dbc84b9c9e434640b38a17d2';
    // var apiLink2 = '/data_analysis_path.aspx?Type=2&Sday=2021-05-05&Eday=2021-06-01&CameraUID=' + cameraId;
    $('.path-result-box').hide();
    $('#pathTable').show();
    $('#pathTable').html('<tr>' +
        '<th> 路徑名稱</th>' +
        '<th>人數</th>' +
        '</tr>');

    $('#pathResult02').show();
    $('#pathB01,#pathB02').html(' - ');
    $('#pathTxtB01,#pathTxtB02').html(' - ');
    //== 電梯口一 ==
    var apiLink = '/api/dataanalysispath?Type=' + unitRange + '&Sday=' + sDay + '&Eday=' + eDay + '&CameraUID=' + cameraId;
    var pathData;
    $.get(apiLink, function (data) {
        var aipData = JSON.parse(data);
        console.log(aipData);
        pathData = aipData;
        //= 匯出csv使用 =
        csvData = aipData;
    });
    console.log('pathData', pathData);
    $.each(pathData, function (ind, val) {
        var { roi_uid, CameraName, Acount, AvgCount } = val;
        // $('#pathB01').html(Number(AvgCount).toFixed(2) + '%');
        // $('#pathTxtB01').html(CameraName);
        $('#pathTable').append('<tr>' +
            '<th>電梯口一 - ' + CameraName + '</th>' +
            '<th>' + FormatNumber(Acount) + '</th>' +
            '</tr>');
        if (roi_uid == '6878611cc9a84a2c9921625827611677') {//CameraName == '往展覽室C'
            $('#pathB01').html(Number(AvgCount).toFixed(2) + '%');
            $('#pathTxtB02').html('電梯口一 ' + CameraName);
        }
    });
    // $('#pathB01').html(Number(pathData[0].AvgCount).toFixed(2) + '%');
    // $('#pathTxtB02').html('電梯口一 ' + pathData[0].CameraName);
    //== 電梯口二 ==    
    var apiLink = '/api/dataanalysispath?Type=' + unitRange + '&Sday=' + sDay + '&Eday=' + eDay + '&CameraUID=' + cameraId2;
    var pathData2;
    $.get(apiLink, function (data) {
        var aipData = JSON.parse(data);
        console.log(aipData);
        pathData2 = aipData;
        //= 匯出csv使用 =
        csvData2 = aipData;
    });
    console.log('pathData2', pathData2);
    $.each(pathData2, function (ind, val) {
        var { roi_uid, CameraName, Acount, AvgCount } = val;
        // $('#pathB02').html(Number(AvgCount).toFixed(2) + '%');
        // $('#pathTxtB02').html(CameraName);
        $('#pathTable').append('<tr>' +
            '<th>電梯口二 - ' + CameraName + '</th>' +
            '<th>' + FormatNumber(Acount) + '</th>' +
            '</tr>');
        if (roi_uid == 'f8f9824543e24613a291625825932555') {//CameraName == '往大廳'
            $('#pathB02').html(Number(AvgCount).toFixed(2) + '%');
            $('#pathTxtB01').html('電梯口二 ' + CameraName);
        }
    });
    // $('#pathB02').html(Number(pathData2[1].AvgCount).toFixed(2) + '%');
    // $('#pathTxtB01').html('電梯口二 ' + pathData2[1].CameraName);
}

//=== 千分位 ===
function FormatNumber(n) {
    n += "";
    var arr = n.split(".");
    var re = /(\d{1,3})(?=(\d{3})+$)/g;
    return arr[0].replace(re, "$1,") + (arr.length == 2 ? "." + arr[1] : "");
}

//=== csv匯出 ===
//匯出功能
function urlTableToExcel() {
    //要匯出的json資料 urlCsvTable
    //urlCsvTable
    //列標題，逗號隔開，每一個逗號就是隔開一個單元格
    var urlCsvTable = [];
    if (csvData2.length !== 0) {
        $.each(csvData, function (ind, val) {
            var { CameraName, Acount } = val;
            urlCsvTable.push({
                'info1': '電梯口一 - ' + CameraName,
                'info2': Acount,
            });
        });
        $.each(csvData2, function (ind, val) {
            var { CameraName, Acount } = val;
            urlCsvTable.push({
                'info1': '電梯二 - ' + CameraName,
                'info2': Acount,
            });
        });
    } else {
        $.each(csvData, function (ind, val) {
            var { CameraName, Acount } = val;
            urlCsvTable.push({
                'info1': CameraName,
                'info2': Acount,
            });
        });
    }

    // let str = `總計,瀏覽量,不重複瀏覽數,使用者,平均工作階段時間長度,單次工作階段頁數,跳出率\t,\n`;
    // str += ' ,' + t1 + ',' + t2 + ',' + t3 + ',' + t4 + ',' + t5 + ',' + t6 + '\t,\n';
    //空白格
    // str += ' \t,\n';
    //頁面,瀏覽量,不重複瀏覽量,平均頁面停留時間,入口,跳出率,離開率
    // str += `頁面,瀏覽量,不重複瀏覽量,平均頁面停留時間,入口,跳出率,離開率\n`;
    let str = `路徑名稱,人數\n`;
    // let str = `Page,Pageviews,Unique Pageviews,Avg. Time on Page,Entrances,Bounce Rate,% Exit\n`;
    // let str = `姓名,電話,郵箱\n`;
    //增加\t為了不讓表格顯示科學計數法或者其他格式
    for (let i = 0; i < urlCsvTable.length; i++) {
        for (let item in urlCsvTable[i]) {
            str += `${urlCsvTable[i][item] + '\t'},`;
        }
        str += '\n';
    }
    //encodeURIComponent解決中文亂碼
    let uri = 'data:text/csv;charset=utf-8,\ufeff' + encodeURIComponent(str);
    //通過建立a標籤實現
    var link = document.createElement("a");
    link.href = uri;
    //對下載的檔案命名
    // var dateString = $('#starTime').val().split(' ~ ');
    // var startDay = dateString[0];
    // var endDay = dateString[1];
    // if (!endDay) {
    //     //如果沒有選擇結束日
    //     var csvName = "data_" + startDay;
    // } else {
    //     var csvName = "data_" + startDay + '_' + endDay;
    // }
    link.download = csvFileName + ".csv";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}