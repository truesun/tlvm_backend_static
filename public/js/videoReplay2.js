// const mediaArray = [
//     {
//         "server": "https://oregon-p1-stage-ss-edge-1-2.beseye.com:1934/playlist",
//         "stream": "{enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHCH-t9ZhdD_GKwJwf1SVphRr9e4eG9birYOPdF-Si7IPKJ2hKX77oJEgvhpmNahZnJiSdDhKHSZBDDWLC1eHUyAlhyQqSMEEnUsXhg7PErMkw==/manifest.mpd",
//         "startTime": "1622540027470",
//         "duration": "10013",
//         "manifestUri": "https://oregon-p1-stage-ss-edge-1-2.beseye.com:1934/playlist?host=oregon-p1-stage-ss-edge-1-2.beseye.com&token=8438c3cc0e4f8bf3c7cbf18ceba36c05e025a9dca9789ebdc08483cd78917f8c&dd={Web}_{KW-External}_{12345678}&vci=ea8e1f99fc4d47ca8e7fc3936a19e9de&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHCH-t9ZhdD_GKwJwf1SVphRr9e4eG9birYOPdF-Si7IPKJ2hKX77oJEgvhpmNahZnJiSdDhKHSZBDDWLC1eHUyAlhyQqSMEEnUsXhg7PErMkw==/manifest.mpd",
//     },
//     {
//         "server": "https://oregon-p1-stage-ss-edge-1-2.beseye.com:1934/playlist",
//         "stream": "{enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHCH-t9ZhdD_GKwJwf1SVphRr9e4eG9birYOPdF-Si7IPMkj5SVoaFqVSnr7IAI8h1GtIESRd-XEy6xSoFauvLCYHcGKqx880bFZYonF_dYXJA==/manifest.mpd",
//         "startTime": "1622540037321",
//         "duration": "10052",
//         "manifestUri": "https://oregon-p1-stage-ss-edge-1-2.beseye.com:1934/playlist?host=oregon-p1-stage-ss-edge-1-2.beseye.com&token=8438c3cc0e4f8bf3c7cbf18ceba36c05e025a9dca9789ebdc08483cd78917f8c&dd={Web}_{KW-External}_{12345678}&vci=ea8e1f99fc4d47ca8e7fc3936a19e9de&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHCH-t9ZhdD_GKwJwf1SVphRr9e4eG9birYOPdF-Si7IPMkj5SVoaFqVSnr7IAI8h1GtIESRd-XEy6xSoFauvLCYHcGKqx880bFZYonF_dYXJA==/manifest.mpd",
//     },
// ]

var playingPlayer2;
var video3 = document.getElementById('video3');
var video4 = document.getElementById('video4');
var player3 = undefined
var player4 = undefined
var players2 = {}
var playlist2 = []
var isPlaying2 = false
// == 啟動回放 ==
// document.addEventListener('DOMContentLoaded', initAppTwo(mediaArray));

function checkUndefinedTwo(x) {
    return typeof x == "undefined"
}

function initAppTwo(data) {
    console.log('=== 1.initAppTwo ===');

    // Install built-in polyfills to patch browser incompatibilities.
    shaka.polyfill.installAll()

    // Check to see if the browser supports the basic APIs Shaka needs.
    if (shaka.Player.isBrowserSupported()) {
        // Everything looks good!
        initPlayerTwo()
    } else {
        // This browser does not have the minimum set of APIs we need.
        console.error('Browser not supported!')
    }
    updatePlaylistTwo(data)
}

function initPlayerTwo() {
    console.log('=== 2.initPlayerTwo ===');
    // Create a Player instance.
    video4.style.display = 'none';
    player3 = new shaka.Player(video3);
    player4 = new shaka.Player(video4);
    player3.video = video3
    player4.video = video4
    player3.shouldPlay = false
    player4.shouldPlay = false
    player3.firstPlay = false
    player4.firstPlay = false
    players2 = { mainPlayer: player3, preloadPlayer: player4 }
    // window.player1 = player1
    // window.player2 = player2
    setupPlayerEventListenersTwo()
    setupVideoEventListenersTwo()
}

function setupPlayerEventListenersTwo() {
    console.log('=== 3.setupPlayerEventListenersTwo ===');
    // shaka player events: [error]
    player3.addEventListener('error', onPlayerErrorTwo)
    player4.addEventListener('error', onPlayerErrorTwo)
}

function setupVideoEventListenersTwo() {
    console.log('=== 4.setupVideoEventListenersTwo ===');
    // video events: [play,canplay,emptied,ended,error...]
    player3.video.addEventListener('play', onVideoPlayTwo.bind(player3))
    player3.video.addEventListener('ended', onVideoEndedTwo.bind(player3))
    player3.video.addEventListener('timeupdate', onVideoTimeUpdateTwo.bind(player3))
    player4.video.addEventListener('play', onVideoPlayTwo.bind(player4))
    player4.video.addEventListener('ended', onVideoEndedTwo.bind(player4))
    player4.video.addEventListener('timeupdate', onVideoTimeUpdateTwo.bind(player4))
}

//  --------------------------------------------
//  Shaka player event
//  --------------------------------------------
function onPlayerErrorTwo() {
    // Extract the shaka.util.Error object from the event.
    onErrorTwo(event.detail)
}

function onErrorTwo(error) {
    console.log('=== 5.onErrorTwo ===');
    console.error('Error code', error.code, 'object', error,)
    alert('Error code: ' + error.code + '\nError: ' + error,)
}

function onPlayerLoadedTwo() {
    console.log('=== 6.onPlayerLoadedTwo ===');
    var player = this
    console.log('onPlayerLoadedTwo', player.video)
    player.loaded = true
    player.video.play()
}

//  --------------------------------------------
//  Video event
//  --------------------------------------------
function onVideoPlayTwo() {
    console.log('=== 7.onVideoPlayTwo ===');
    var player = this
    console.log('onVideoPlayTwo', player.video)
    player.firstPlay = true
}

function onVideoEndedTwo() {
    console.log('=== 8.onVideoEndedTwo ===');
    console.log('onVideoEndedTwo', this.video)
    // switch player
    var mainPlayer = players2.preloadPlayer
    var preloadPlayer = players2.mainPlayer
    preloadPlayer.media = undefined
    preloadPlayer.ready = false
    players2.preloadPlayer = preloadPlayer
    players2.mainPlayer = mainPlayer
    if (mainPlayer.ready) {
        mainPlayer.video.play()
    } else {
        preloadPlayer.video.style.display = 'none'
    }
    // preload player load new streaming
    setTimeout(preparePreloadStreamingTwo, 1000)
}

function onVideoTimeUpdateTwo() {
    console.log('=== 9.onVideoTimeUpdateTwo ===');
    player = this
    if (!player.loaded) {
        console.log('=== 9.player.loaded ===');
        return
    }
    if (player == players2.preloadPlayer) {
        console.log('onVideoTimeUpdateTwo preload player', player.video)
        player.video.pause()
        player.ready = true
    } else {
        if (player.firstPlay) {
            player.firstPlay = false
            console.log('onVideoTimeUpdateTwo main player firstPlay', player.video)
            showHideVideoTwo()
            // [custom] onPlay callback should be called to change UI
        }
        if (isPlaying2) {
            // [custom] timer callback should be called to update time
        }
    }
}

//  --------------------------------------------
//   main action
//  --------------------------------------------
function updatePlaylistTwo(newPlaylist) {
    console.log('=== 10.updatePlaylistTwo ===');
    var mainPlayer = players2.mainPlayer
    var preloadPlayer = players2.preloadPlayer
    if (newPlaylist.length == 0) {
        console.error('Got empty playlist')
        return
    }
    if (isPlaying2) {
        if (playlist2 == 0 && checkUndefinedTwo(preloadPlayer.media)) {
            preloadPlayer.media = newPlaylist[0]
            preloadStreamingTwo(preloadPlayer, newPlaylist[0].manifestUri)
            newPlaylist.shift()
        }
        playlist2 = playlist2.concat(newPlaylist)
    } else {
        // before play
        isPlaying2 = true
        mainPlayer.ready = false
        preloadPlayer.ready = false
        for (let index = 0; index < newPlaylist.length; index++) {
            const media = newPlaylist[index];
            switch (index) {
                case 0:
                    mainPlayer.media = media
                    loadStreamingTwo(mainPlayer, media.manifestUri)
                    break;
                case 1:
                    preloadPlayer.media = media
                    setTimeout(() => {
                        preloadStreamingTwo(preloadPlayer, media.manifestUri)
                    }, 1000);
                    break;
                default:
                    playlist2.push(media)
                    break;
            }

        }
        // [custom] count player end time
    }
}

function preparePreloadStreamingTwo() {
    console.log('=== 11.preparePreloadStreamingTwo ===');
    if (playlist2.length == 0) {
        // to-da request new playlist
        console.warn('Out of playlist2')
    } else {
        preloadPlayer = players2.preloadPlayer
        preloadPlayer.media = playlist2[0]
        preloadStreamingTwo(preloadPlayer, playlist2[0].manifestUri)
        playlist2.shift()
    }
}

function preloadStreamingTwo(player, assetUri, startTime = null, mimeType = 'application/dash+xml') {
    console.log('=== 12.preloadStreamingTwo ===');
    player.shouldPlay = false
    loadStreamingTwo(player, assetUri, startTime, mimeType)
}

function loadStreamingTwo(player, assetUri, startTime = null, mimeType = 'application/dash+xml') {
    console.log('=== 13.loadStreamingTwo ===');
    console.log('loadStreamingTwo', '\nplayer', player.video, '\nmedia:', player.media, '\nUri:', assetUri, '\nplaylist:', playlist2)
    if (assetUri !== '') {
        player.loaded = false
        player.load(assetUri, startTime, mimeType).then(onPlayerLoadedTwo.bind(player)).catch(onErrorTwo)
    } else {
        // no clip
        var lastDuration = getPlaylistDurationTwo()
        console.log('no clips happened', players2, playlist2)
        // [custom] callback to play live after lastDuration
    }

}

//  --------------------------------------------
//   User action
//  --------------------------------------------

function captureSnapshotTwo() {
    var video = players2.mainPlayer.video
    canvas = document.createElement('canvas')
    canvas.width = 1920
    canvas.height = 1080
    ctx = canvas.getContext('2d')
    ctx.drawImage(video, 0, 0, canvas.width, canvas.height)
    dataURI = canvas.toDataURL('image/jpeg')
    link = document.createElement('a')
    link.download = 'snapshot.png'
    link.href = dataURI
    link.click()
}

//  --------------------------------------------
//   UI control
//  --------------------------------------------

function showHideVideoTwo() {
    console.log('=== 14.showHideVideoTwo ===');
    players2.mainPlayer.video.style.display = 'block'
    players2.preloadPlayer.video.style.display = 'none'
}


//  --------------------------------------------
//   helper function
//  --------------------------------------------
function getPlaylistDurationTwo() {
    console.log('=== 15.getPlaylistDurationTwo ===');
    var lastDuration = 0
    for (let index = 0; index < playlist2.length; index++) {
        const media = playlist2[index];
        lastDuration += media.duration
    }
    if (!checkUndefinedTwo(players2.preloadPlayer.media)) {
        lastDuration += players2.preloadPlayer.media.duration
    }
    if (!checkUndefinedTwo(players2.mainPlayer.media)) {
        lastDuration += players2.mainPlayer.media.duration - players2.mainPlayer.video.currentTime * 1000
    }
    return lastDuration
}

    // document.addEventListener('DOMContentLoaded', initApp);
// }
