$(function () {
    // $.ajax({
    //     url: 'http://api.map.baidu.com/location/ip?ak=ia6HfFL660Bvh43exmH9LrI6',
    //     type: 'POST',
    //     dataType: 'jsonp',
    //     success: function (data) {
    //         console.log(data.content);
    //         $('#ip').html(data.content.address_detail.province + "," + data.content.address_detail.city);
    //     }
    // });
    // $('#ip').html(returnCitySN["cip"]);
    // var returnCitySN = { "cip": "36.225.144.38", "cid": "710000", "cname": "台湾省" };
    // $('#ip').html(returnCitySN.cip + ',' + returnCitySN.cid + ',' + returnCitySN.cname);
    // console.log(returnCitySN.cip, returnCitySN.cid, returnCitySN.cname)
    var yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    //=== 日期格式 ===dateRange
    $("#dateRange").datepicker({
        dateFormat: "yyyy/mm/dd",
        autoClose: true,
        position: "bottom right",
        language: 'en',
        range: true,
        multipleDatesSeparator: ' ~ ',
        maxDate: yesterday,
        // minDate: threeMonth,
        onSelect: (fd, d, picker) => {
            //- this.date = this.$("#date").val();
        },
    });
    // var currentDate = new Date();
    $("#dateRange").data('datepicker').selectDate(yesterday);
    // $("#dateRange").data('datepicker').selectDate(new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()))

    //取消因為滾動不會消失的日期選取器
    $('.main').scroll(function () {
        $('.datepickers-container').find('.datepicker').removeClass('active');
        $('.datepickers-container').find('.datepicker').css({ 'left': '-100000px' });
        $('#dateRange').blur();
    });
    //= 初始圖表 =
    srcChartStat();
    $('#srcBtn').on('click', function () {
        //= 搜尋圖表 =
        srcChartStat();
    });

    // //=== 取得API ===
    // var apiLink = '/data_analysis.aspx?Type=1&sday=2021-05-17&eday=2021-05-17';
    // // var dataListT = [];
    // var dataList;
    // //= 取得搜尋日期 =
    // var dataDateList = [];
    // //= 取得攝影機列表 =
    // var cameraNameList = [];
    // $.get(apiLink, function (data) {
    //     dataList = JSON.parse(data);
    //     // console.log(dataList);
    //     $.each(dataList, function (ind, val) {
    //         var { timestamp, CameraName, Acount } = val;
    //         //放入搜尋時間
    //         var hasInd = dataDateList.findIndex(function (item) {
    //             return item == timestamp;
    //         });
    //         if (hasInd == -1) {
    //             dataDateList.push(timestamp);
    //         }
    //         //放入攝影機名稱
    //         var hasCamInd = cameraNameList.findIndex(function (item) {
    //             return item == CameraName;
    //         });
    //         if (hasCamInd == -1) {
    //             cameraNameList.push(CameraName);
    //         }


    //         // var timestampStr = timestamp.split(' ');
    //         // // console.log(timestampStr);
    //         // dataListT.push({ 'date': timestampStr[0], 'time': timestampStr[1], 'CameraName': CameraName, 'Acount': Acount })
    //     });
    // });
    // console.log(dataDateList);
    // console.log(cameraNameList);
    // var dataDate = [];
    // $.each(dataDateList, function (ind, val) {
    //     var timestampStr = val.split(' ');
    //     var day = timestampStr[0];
    //     day = day.split('-');
    //     var dayS = day[1] + '/' + day[2];
    //     if (timestampStr.length > 1) {
    //         //判斷是否為小時
    //         var time = timestampStr[1];
    //         time = time.replace(':00:00.00', '');
    //     }
    //     dataDate.push(dayS + ' ' + time);
    // });
    // //= 圖表資料 =
    // var chartData = [];
    // // //= 取得搜尋日期 =
    // // var dataDateList = [];
    // // console.log(dataListT);
    // //"Total" ,"台文館正門","台文館咖啡廳","台文館客情","台文館電梯口-1","台文館電梯口-2","串流串接攝影機"
    // $.each(cameraNameList, function (ind, valName) {
    //     var list = dataList.filter(function (item) {
    //         return item.CameraName == valName;
    //     });
    //     //= 總計點點 =
    //     var dataDot = [];
    //     $.each(list, function (ind, val) {
    //         var { Acount } = val;
    //         //== data ==
    //         dataDot.push(Acount);
    //     });
    //     chartData.push({ 'name': valName, 'data': dataDot });
    // });


    // var totalList = dataList.filter(function (item) {
    //     return item.CameraName == cameraNameList[0];
    // });
    // //= 總計點點 =
    // var dataTotalDot = [];
    // $.each(totalList, function (ind, val) {
    //     var { date, Acount } = val;
    //     // 列出時間列表
    //     var hasInd = dataDateList.findIndex(function (item) {
    //         return item == date;
    //     });
    //     if (hasInd == -1) {
    //         dataDateList.push(date);
    //     }
    //     //== data ==
    //     dataTotalDot.push(Acount);
    // });
    // chartData.push({ 'name': cameraNameList[0], 'data': dataTotalDot });

    // 圖表日期區間
    // var dataDate = [];
    // $.each(dataDateList, function (ind, val) {
    //     console.log('=== 看這喵喵 ===');
    //     console.log(val);
    //     // var day = val.split('-');
    //     // for (i = 0; i < 24; i++) {
    //     //     if (i < 10) {
    //     //         i = '0' + i;
    //     //     }
    //     //     dataDate.push(day[1] + '/' + day[2] + ' ' + i + ':00');
    //     //     // dataDate.push(val + ' ' + i);
    //     // }
    // });
    // console.log(dataDate);


    // var cameraList1 = dataListT.filter(function (item) {
    //     return item.CameraName == '台文館正門';
    // });
    // //= 總計點點 =
    // var datacameraDot1 = [];
    // $.each(cameraList1, function (ind, val) {
    //     var { Acount } = val;
    //     //== data ==
    //     datacameraDot1.push(Acount);
    // });
    // chartData.push({ 'name': '台文館正門', 'data': datacameraDot1 });

    // var cameraList2 = dataListT.filter(function (item) {
    //     return item.CameraName == '台文館咖啡廳';
    // });
    // //= 總計點點 =
    // var datacameraDot2 = [];
    // $.each(cameraList2, function (ind, val) {
    //     var { Acount } = val;
    //     //== data ==
    //     datacameraDot2.push(Acount);
    // });
    // chartData.push({ 'name': '台文館咖啡廳', 'data': datacameraDot2 });

    // var cameraList3 = dataListT.filter(function (item) {
    //     return item.CameraName == '台文館客情';
    // });
    // //= 總計點點 =
    // var datacameraDot3 = [];
    // $.each(cameraList3, function (ind, val) {
    //     var { Acount } = val;
    //     //== data ==
    //     datacameraDot3.push(Acount);
    // });
    // chartData.push({ 'name': '台文館客情', 'data': datacameraDot3 });

    // var cameraList4 = dataListT.filter(function (item) {
    //     return item.CameraName == '台文館電梯口-1';
    // });
    // //= 總計點點 =
    // var datacameraDot4 = [];
    // $.each(cameraList4, function (ind, val) {
    //     var { Acount } = val;
    //     //== data ==
    //     datacameraDot4.push(Acount);
    // });
    // chartData.push({ 'name': '台文館電梯口-1', 'data': datacameraDot4 });

    // var cameraList5 = dataListT.filter(function (item) {
    //     return item.CameraName == '台文館電梯口-2';
    // });
    // //= 總計點點 =
    // var datacameraDot5 = [];
    // $.each(cameraList5, function (ind, val) {
    //     var { Acount } = val;
    //     //== data ==
    //     datacameraDot5.push(Acount);
    // });
    // chartData.push({ 'name': '台文館電梯口-2', 'data': datacameraDot5 });

    // var cameraList6 = dataListT.filter(function (item) {
    //     return item.CameraName == '串流串接攝影機';
    // });
    // //= 總計點點 =
    // var datacameraDot6 = [];
    // $.each(cameraList6, function (ind, val) {
    //     var { Acount } = val;
    //     //== data ==
    //     datacameraDot6.push(Acount);
    // });
    // chartData.push({ 'name': '串流串接攝影機', 'data': datacameraDot6 });


    // console.log('=== 圖表資料 ===');
    // console.log(chartData);

    // startChart(dataDate, chartData);
});
//啟動搜尋
function srcChartStat() {
    var type = $('#range').val();
    var dateRange = $('#dateRange').val();
    var sday, eday;
    var dateRangeL = dateRange.split(' ~ ');
    sday = dateRangeL[0];
    dateRangeL.length > 1 ? eday = dateRangeL[1] : eday = dateRangeL[0];
    sday = sday.split('/').join('-');
    eday = eday.split('/').join('-');
    console.log('type:', type);
    console.log('dateRange:', sday, eday);

    srcChart(type, sday, eday);
}
//= 取的API及資料整理 =
function srcChart(type, sday, eday) {
    //=== 取得API ===
    var apiLink = '/api/DataAnalysis?Type=' + type + '&sday=' + sday + '&eday=' + eday;
    // var apiLink = '/data_analysis.aspx?Type=1&sday=2021-05-17&eday=2021-05-17';
    // var dataListT = [];
    var dataList;
    //= 取得搜尋日期 =
    var dataDateList = [];
    //= 取得攝影機列表 =
    var cameraNameList = [];
    $.get(apiLink, function (data) {
        dataList = JSON.parse(data);
        console.log(dataList);
        $.each(dataList, function (ind, val) {
            var { timestamp, CameraName, Acount } = val;
            //放入搜尋時間
            var hasInd = dataDateList.findIndex(function (item) {
                return item == timestamp;
            });
            if (hasInd == -1) {
                dataDateList.push(timestamp);
            }
            //放入攝影機名稱
            var hasCamInd = cameraNameList.findIndex(function (item) {
                return item == CameraName;
            });
            if (hasCamInd == -1) {
                cameraNameList.push(CameraName);
            }
        });
    });
    console.log(dataDateList);
    console.log(cameraNameList);
    var dataDate = [];
    $.each(dataDateList, function (ind, val) {
        var timestampStr = val.split(' ');
        var day = timestampStr[0];
        day = day.split('-');
        //2021-06-09-2021-06-14 周，月
        if (type == 3 || type == 4) {
            var dayS = day[1] + '/' + day[2] + '-' + day[4] + '/' + day[5];
            dataDate.push(dayS);
        } else if (type == 2 || type == 5 || type == 6 || type == 7 || type == 8 || type == 9 || type == 10) {
            //2021-06-09 日 日 上下午區間
            var dayS = day[1] + '/' + day[2];
            dataDate.push(dayS);
        } else if (type == 1) {
            //2021-06-09 時
            var dayS = day[1] + '/' + day[2];
            var time = timestampStr[1];
            time = time.replace(':00:00.00', '');
            dataDate.push(dayS + ' ' + time);
        }
        // var dayS = day[1] + '/' + day[2];
        // if (timestampStr.length > 1) {
        //     //判斷是否為小時
        //     var time = timestampStr[1];
        //     time = time.replace(':00:00.00', '');
        //     dataDate.push(dayS + ' ' + time);
        // } else {
        //     dataDate.push(dayS);
        // }
    });
    //= 圖表資料 =
    var chartData = [];
    $.each(cameraNameList, function (ind, valName) {
        var list = dataList.filter(function (item) {
            return item.CameraName == valName;
        });
        //= 總計點點 =
        var dataDot = [];
        $.each(list, function (ind, val) {
            var { Acount } = val;
            //== data ==
            dataDot.push(Number(Acount));
        });
        chartData.push({ 'name': valName, 'data': dataDot });
    });

    console.log('=== 圖表資料 ===');
    console.log(chartData);

    //== 即時在館人數 ==
    startChart(dataDate, chartData);
    //= 取得在館人數api和製圖表 =
    getRealTimeData();
    //= 五分更新在館人數 =
    reStartRealTime();
}
//== 五分更新在館人數 ==
function reStartRealTime() {
    var today = new Date();
    var hh = today.getHours();
    var mm = today.getMinutes();
    var ss = today.getSeconds();
    mm = checkTime(mm);
    ss = checkTime(ss);
    // $('h2').html(hh + ":" + mm + ":" + ss);
    // console.log(ss);
    //= 更新時間為 09 ~ 18 =
    // if (hh > 9 && (hh < 19 && mm == '00')){
    // }
    if (mm == 00 && ss == 02 || mm == 05 && ss == 02 || mm == 10 && ss == 02 || mm == 15 && ss == 02 || mm == 20 && ss == 02 || mm == 25 && ss == 02 || mm == 30 && ss == 02 || mm == 35 && ss == 02 || mm == 40 && ss == 02 || mm == 45 && ss == 02 || mm == 50 && ss == 02 || mm == 55 && ss == 02) {
        console.log('五分鐘更新');
        getRealTimeData();
    }
    // document.getElementById('clock').innerHTML = hh + ":" + mm + ":" + ss;
    var timeoutId = setTimeout(reStartRealTime, 1000);
}

function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}
//= 製作圖表 =
function startChart(yData, chartData) {
    // A point click event that uses the Renderer to draw a label next to the point
    // On subsequent clicks, move the existing label instead of creating a new one.
    Highcharts.addEvent(Highcharts.Point, 'click', function () {
        if (this.series.options.className.indexOf('popup-on-click') !== -1) {
            const chart = this.series.chart;
            // const date = Highcharts.dateFormat('%A, %b %e, %Y', this.x);
            // const text = `<b>${date}</b><br/>${this.y} ${this.series.name}`;
            const text = `<b>${this.category}</b><br/>${this.series.name} : ${this.y} 人`;

            const anchorX = this.plotX + this.series.xAxis.pos;
            const anchorY = this.plotY + this.series.yAxis.pos;
            const align = anchorX < chart.chartWidth - 200 ? 'left' : 'right';
            const x = align === 'left' ? anchorX + 10 : anchorX - 10;
            const y = anchorY - 30;
            if (!chart.sticky) {
                chart.sticky = chart.renderer
                    .label(text, x, y, 'callout', anchorX, anchorY)
                    .attr({
                        align,
                        fill: 'rgba(0, 0, 0, 0.75)',
                        padding: 10,
                        zIndex: 7 // Above series, below tooltip
                    })
                    .css({
                        color: 'white'
                    })
                    .on('click', function () {
                        chart.sticky = chart.sticky.destroy();
                    })
                    .add();
            } else {
                chart.sticky
                    .attr({ align, text })
                    .animate({ anchorX, anchorY, x, y }, { duration: 250 });
            }
        }
    });

    //chart color
    Highcharts.setOptions({
        colors: ['#7cb4ec', '#f7ce6b', '#91ed7d', '#f7a45b', '#8085e8', '#f25d80'],
        // colors: ['#ed6e85', '#f1a354', '#f7ce6b', '#6dbebf', '#57a1e5', '#de82df'],
        // #7cb4ec #000 #91ed7d #f7a45b #8085e8 #f25d80
        //資料千分位
        lang: {
            thousandsSep: ','
        }
    });

    Highcharts.chart('container', {

        chart: {
            scrollablePlotArea: {
                minWidth: 700
            }
        },

        //   data: {
        //     csvURL: '../data/analytics.csv',
        //     beforeParse: function (csv) {
        //       return csv.replace(/\n\n/g, '\n');
        //     }
        //   },

        title: {
            text: '每日統計'
        },

        subtitle: {
            text: '不包含當日人數、閉館日設定日期之人數'
        },

        xAxis: {
            categories: yData
            // categories: ['4/22', '4/23', '4/24', '4/25', '4/26', '4/27', '4/28', '4/29', '4/30', '5/1', '5/2', '5/3', '5/4', '5/5', '5/6', '5/7', '5/8', '5/9', '5/10', '5/11', '5/12', '5/13', '5/14', '5/15', '5/16', '5/17', '5/18', '5/19', '5/20', '5/21']
        },

        yAxis: [{ // left y axis
            title: {
                text: null
            },
            labels: {
                align: 'left',
                x: 3,
                y: 16,
                format: '{value:.,0f}'
            },
            showFirstLabel: false
        },
        { // right y axis
            linkedTo: 0,
            gridLineWidth: 0,
            opposite: true,
            title: {
                text: null
            },
            labels: {
                align: 'right',
                x: -3,
                y: 16,
                format: '{value:.,0f}'
            },
            showFirstLabel: false
        }],

        legend: {
            // layout: 'vertical',//水平對齊
            align: 'right',// 圖表點點資料左右
            verticalAlign: 'top',
            // align: 'left',
            // verticalAlign: 'top',
            // borderWidth: 0
        },
        //顯示資料table
        tooltip: {
            shared: true,
            crosshairs: true,
            valueSuffix: '<span style="font-size: 8px">人</span>',
            // headerFormat: '<small>日期:{point.key}</small><br>',
            pointFormat: '<span style="color:{point.color}">{series.name}</span> : <b>{point.y}</b><br>'
            // pointFormat: '<b>{point.x} :</b>' + 'Count: <b>{point.y:,.0f}</b>',
        },
        //點擊點點資料顯示
        plotOptions: {
            series: {
                cursor: 'pointer',
                className: 'popup-on-click',
                marker: {
                    lineWidth: 1
                }
            }
        },
        credits: {
            enabled: false
        },
        //右上角功能按鈕
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: ["viewFullscreen", "printChart", "separator", "downloadPNG", "downloadJPEG", "downloadPDF", "downloadSVG", "separator", "downloadCSV", "downloadXLS"],
                }
            }
        },
        //資料
        series: chartData
        // series: [
        //     {
        //         name: '正大門攝影A',
        //         data: [1299, 828, 2345, 134, 462, 234, 533, 808, 846, 426, 656, 869, 908, 597, 457, 654, 855, 890, 456, 533, 808, 846, 426, 656, 134, 462, 234, 533, 462, 234]
        //     },
        //     {
        //         name: '正大門攝影B',
        //         data: [908, 597, 457, 654, 855, 890, 456, 533, 808, 846, 426, 656, 134, 462, 234, 533, 462, 234, 533, 808, 1299, 828, 2345, 134, 462, 234, 533, 808, 846, 426]
        //     },
        //     {
        //         name: '電梯口',
        //         data: [533, 808, 846, 426, 656, 869, 908, 597, 457, 654, 855, 890, 456, 1299, 828, 2345, 134, 462, 234, 533, 808, 846, 426, 656, 134, 462, 234, 533, 462, 234]
        //     },
        //     {
        //         name: '文化資產保存研究中心門口',
        //         data: [908, 597, 457, 654, 855, 890, 456, 533, 1299, 828, 2345, 134, 462, 234, 533, 808, 846, 426, 656, 869, 808, 846, 426, 656, 134, 462, 234, 533, 462, 234]
        //     },
        //     {
        //         name: '展覽室D即時影像',
        //         data: [597, 457, 654, 855, 890, 456, 533, 1299, 828, 2345, 134, 462, 234, 533, 808, 846, 426, 656, 869, 908, 597, 457, 654, 855, 890, 456, 533, 808, 846, 426]
        //     },
        //     {
        //         name: '全部總數',
        //         data: [5676, 6473, 6537, 6653, 6546, 6535, 5333, 1299, 8328, 2435, 1324, 4362, 2324, 5332, 8083, 8346, 4226, 6526, 8269, 9082, 5917, 4517, 6254, 8255, 1890, 1231, 1245, 3256, 2364, 6413]
        //     }
        // ]
    });
}
//= 取得在館人數api =
function getRealTimeData() {
    var realTimeApiLink = '/Api/GetGroup';
    var realTimeDataList, realTimeDataListArray;
    var realTimedataDot = []
    var realTimechartMin = [];
    var realTimechartData = [];
    $.get(realTimeApiLink, function (data) {
        console.log('== 即時在館人數 ==');
        realTimeDataList = JSON.parse(data);
        //== 館內人數顯示 ==
        var realTimeNum = realTimeDataList.Alltotalcount;
        $('#realTimeNum').html(realTimeNum);
        if (Number(realTimeNum) > 500) {
            $('#realTimeBox').addClass('real-people03');
        } else if (Number(realTimeNum) > 100) {
            $('#realTimeBox').addClass('real-people02');
        } else {
            $('#realTimeBox').addClass('real-people01');
        }
        //= 在館人數時間資料整理 =
        realTimeDataListArray = JSON.parse(realTimeDataList.data.Array);
        $.each(realTimeDataListArray, function (ind, val) {
            var { timestamp, totalcount } = val;
            realTimedataDot.push(Number(totalcount));
            var timeList = timestamp.split(' ');
            var minData = timeList[1];
            minData = minData.replace(':00', '');
            realTimechartMin.push(minData);
        });
        realTimechartData.push({ 'name': '人數', 'data': realTimedataDot });
        // console.log(realTimechartData);
        // console.log(realTimechartMin);
    });
    //== 在館人數 ==
    startRealTimeChart(realTimechartMin, realTimechartData);
    //== 更新時間 ==
    var showToday = new Date();
    var yy = showToday.getFullYear();
    var mon = showToday.getMonth() + 1;
    var dd = showToday.getDate();
    var hh = showToday.getHours();
    var mm = showToday.getMinutes();
    hh = checkTime(hh);
    mm = checkTime(mm);
    $("#upDataTime").html(yy + "/" + mon + "/" + dd + " " + hh + ":" + mm);
}
//= 即時在館圖表 =
function startRealTimeChart(yData, chartData) {
    Highcharts.chart('realPeople', {

        chart: {
            scrollablePlotArea: {
                minWidth: 700
            }
        },

        //   data: {
        //     csvURL: '../data/analytics.csv',
        //     beforeParse: function (csv) {
        //       return csv.replace(/\n\n/g, '\n');
        //     }
        //   },

        title: {
            text: '即時數據'
        },

        subtitle: {
            text: '目前館內人數'
        },

        xAxis: {
            categories: yData
            // categories: ['4/22', '4/23', '4/24', '4/25', '4/26', '4/27', '4/28', '4/29', '4/30', '5/1', '5/2', '5/3', '5/4', '5/5', '5/6', '5/7', '5/8', '5/9', '5/10', '5/11', '5/12', '5/13', '5/14', '5/15', '5/16', '5/17', '5/18', '5/19', '5/20', '5/21']
        },

        yAxis: [{ // left y axis
            title: {
                text: null
            },
            labels: {
                align: 'left',
                x: 3,
                y: 16,
                format: '{value:.,0f}'
            },
            showFirstLabel: false
        },
        { // right y axis
            linkedTo: 0,
            gridLineWidth: 0,
            opposite: true,
            title: {
                text: null
            },
            labels: {
                align: 'right',
                x: -3,
                y: 16,
                format: '{value:.,0f}'
            },
            showFirstLabel: false
        }],

        legend: {
            // layout: 'vertical',//水平對齊
            align: 'right',// 圖表點點資料左右
            verticalAlign: 'top',
            // align: 'left',
            // verticalAlign: 'top',
            // borderWidth: 0
        },
        //顯示資料table
        tooltip: {
            shared: true,
            crosshairs: true,
            valueSuffix: '<span style="font-size: 8px">人</span>',
            // headerFormat: '<small>日期:{point.key}</small><br>',
            pointFormat: '<span style="color:{point.color}">{series.name}</span> : <b>{point.y}</b><br>'
            // pointFormat: '<b>{point.x} :</b>' + 'Count: <b>{point.y:,.0f}</b>',
        },
        //點擊點點資料顯示
        plotOptions: {
            series: {
                cursor: 'pointer',
                className: 'popup-on-click',
                marker: {
                    lineWidth: 1
                }
            }
        },
        credits: {
            enabled: false
        },
        //右上角功能按鈕
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: ["viewFullscreen", "printChart", "separator", "downloadPNG", "downloadJPEG", "downloadPDF", "downloadSVG", "separator", "downloadCSV", "downloadXLS"],
                }
            }
        },
        //資料
        series: chartData
        // series: [
        //     {
        //         name: '正大門攝影A',
        //         data: [1299, 828, 2345, 134, 462, 234, 533, 808, 846, 426, 656, 869, 908, 597, 457, 654, 855, 890, 456, 533, 808, 846, 426, 656, 134, 462, 234, 533, 462, 234]
        //     },
        //     {
        //         name: '正大門攝影B',
        //         data: [908, 597, 457, 654, 855, 890, 456, 533, 808, 846, 426, 656, 134, 462, 234, 533, 462, 234, 533, 808, 1299, 828, 2345, 134, 462, 234, 533, 808, 846, 426]
        //     },
        //     {
        //         name: '電梯口',
        //         data: [533, 808, 846, 426, 656, 869, 908, 597, 457, 654, 855, 890, 456, 1299, 828, 2345, 134, 462, 234, 533, 808, 846, 426, 656, 134, 462, 234, 533, 462, 234]
        //     },
        //     {
        //         name: '文化資產保存研究中心門口',
        //         data: [908, 597, 457, 654, 855, 890, 456, 533, 1299, 828, 2345, 134, 462, 234, 533, 808, 846, 426, 656, 869, 808, 846, 426, 656, 134, 462, 234, 533, 462, 234]
        //     },
        //     {
        //         name: '展覽室D即時影像',
        //         data: [597, 457, 654, 855, 890, 456, 533, 1299, 828, 2345, 134, 462, 234, 533, 808, 846, 426, 656, 869, 908, 597, 457, 654, 855, 890, 456, 533, 808, 846, 426]
        //     },
        //     {
        //         name: '全部總數',
        //         data: [5676, 6473, 6537, 6653, 6546, 6535, 5333, 1299, 8328, 2435, 1324, 4362, 2324, 5332, 8083, 8346, 4226, 6526, 8269, 9082, 5917, 4517, 6254, 8255, 1890, 1231, 1245, 3256, 2364, 6413]
        //     }
        // ]
    });
}